# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/waimus/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="waimus"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in $ZSH/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# Caution: this setting can cause issues with multiline prompts (zsh 5.7.1 and newer seem to work)
# See https://github.com/ohmyzsh/ohmyzsh/issues/5765
COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

alias la='ls -lha'
alias vim='nvim'
alias apostrophe='flatpak run org.gnome.gitlab.somas.Apostrophe'
alias codium='GTK_THEME=Adwaita:dark codium'

# Neofetch alias with custom ASCII path and colours
# Custom ASCII is available on https://github.com/waimus/ascii-logo 
alias neofetch='neofetch --ascii ~/.ascii/lp.txt --colors 4 6 10 12 --ascii_colors 4 15'
#alias neofetch='neofetch --ascii ~/.ascii/hl.txt --colors 11 11 10 12 --ascii_colors 4'

# Turn off screen, specific for Wayland
alias screenoff='sh -c "sleep 0.3 && busctl --user call org.gnome.Shell /org/gnome/ScreenSaver org.gnome.ScreenSaver SetActive b true"'

# Nvidia Optimus env. Noted from https://rpmfusion.org/Howto/Optimus
alias NVPRIME='__NV_PRIME_RENDER_OFFLOAD=1'                    # Use for Vulkan & OpenGL
alias GLXVENDOR='__GLX_VENDOR_LIBRARY_NAME=nvidia'             # Use for OpenGL 
alias NVIDIAGL='__NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0'  # Use for OpenGL (RPM Fusion: "Finer-grained control of GLX + OpenGL")
alias NVIDIAVK='__VK_LAYER_NV_optimus=NVIDIA_only'             # Use for Vulkan (RPM Fusion: "Finer-grained control of Vulkan")
alias NVIDIANOVK='__VK_LAYER_NV_optimus=non_NVIDIA_only'       # Use for Vulkan (but reports non-NVIDIA GPU to app)

# MESA-INTEL: warning: Performance support disabled, consider sysctl dev.i915.perf_stream_paranoid=0
alias streamparanoia='sudo sysctl dev.i915.perf_stream_paranoid=0'

# Suckless' surf needs to run with X11
alias surf='GDK_BACKEND=x11 surf'

# Godot Flatpak
alias godot='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia flatpak run org.godotengine.Godot'

# Pipewire-Jack
alias pw-jack='PIPEWIRE_LATENCY="128/48000" pw-jack'

# Firejail aliases
alias obs='firejail --noprofile --net=none obs'

# Cat -> highlight
alias cat='bat'

# Steam Locomotive
alias sl='sl -aln'

# GNOME Builder
alias gnome-builder='flatpak run org.gnome.Builder'

# Kate / Qt app: launch with X11
alias kate='QT_QPA_PLATFORM=xcb kate'

/home/waimus/.local/bin/colorpanes.sh
/home/waimus/.local/bin/catpr list --count

export PNPM_HOME="/home/waimus/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
