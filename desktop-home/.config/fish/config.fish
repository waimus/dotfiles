if status is-interactive
    # Commands to run in interactive sessions can go here
end

##############
# PATH SETUP #
##############
set CARGO_BIN $HOME/.cargo/bin
set LOCAL_BIN $HOME/.local/bin
set PYTHONBIN $HOME/.local/lib/python3.9/site-packages
set CBIN /usr/lib64/ccache
set BIN1 /usr/bin
set BIN2 /usr/sbin
set BIN3 /usr/local/bin
set BIN4 /usr/local/sbin
set FLUTTER_BIN /opt/flutter/bin
set ANDROID_BUILD $HOME/.android/sdk/build-tools/33.0.2
set ANDROID_TOOLS $HOME/.android/sdk/platform-tools
set ANDROID_CMDTL $HOME/.android/sdk/cmdline-tools

export PATH=$CARGO_BIN:$LOCAL_BIN:$PYTHONBIN:$CBIN:$BIN1:$BIN2:$BIN3:$BIN4:$FLUTTER_BIN:$ANDROID_BUILD:$ANDROID_TOOLS:$ANDROID_CMDTL:$PATH

###############
# ENVIRONMENT #
###############
export FrameworkPathOverride=/lib/mono/4.5

#############
# FUNCTIONS #
#############
function fish_prompt -d "Write out the prompt"
    # Fish prompt format. ([user@host ~/pwd]$)
    printf '%s[%s%s@%s %s%s%s]%s$%s ' (set_color blue) (set_color $fish_color_user) \
       $USER $hostname \
       (set_color $fish_color_cwd) (prompt_pwd) (set_color blue) \
       (set_color magenta) (set_color normal) 
end

function fish_right_prompt -d "Write out the right prompt"
   # Fish right side prompt
   set -g __fish_git_prompt_showdirtystate auto
   printf '%s' (fish_git_prompt)
end

###################
# COMMAND ALIASES #
###################
alias l='ls -l'
alias la='ls -lha'
alias vim='nvim'
alias apostrophe='flatpak run org.gnome.gitlab.somas.Apostrophe'
#alias codium='GTK_THEME=Adwaita:dark codium'

# Neofetch alias with custom ASCII path and colours
# Custom ASCII is available on https://github.com/waimus/ascii-logo 
alias neofetch='neofetch --ascii ~/.ascii/lp.txt --colors 5 5 10 12 --ascii_colors 8 4'
# alias neofetch='neofetch --ascii ~/.ascii/hl.txt --colors 11 11 10 12 --ascii_colors 4'

# Fastfetch
alias fastfetch='fastfetch --iterm ~/.ascii/img/girl_c.jpg'

# Turn off screen, specific for Wayland
alias screenoff='sh -c "sleep 0.3 && busctl --user call org.gnome.Shell /org/gnome/ScreenSaver org.gnome.ScreenSaver SetActive b true"'

# Nvidia Optimus env. Noted from https://rpmfusion.org/Howto/Optimus
alias NVOFF='__NV_PRIME_RENDER_OFFLOAD=0'
alias NVPRIME='__NV_PRIME_RENDER_OFFLOAD=1'                    # Use for Vulkan & OpenGL
alias GLXVENDOR='__GLX_VENDOR_LIBRARY_NAME=nvidia'             # Use for OpenGL 
alias NVIDIAGL='__NV_PRIME_RENDER_OFFLOAD_PROVIDER=NVIDIA-G0'  # Use for OpenGL (RPM Fusion: "Finer-grained control of GLX + OpenGL")
alias NVIDIAVK='__VK_LAYER_NV_optimus=NVIDIA_only'             # Use for Vulkan (RPM Fusion: "Finer-grained control of Vulkan")
alias NVIDIANOVK='__VK_LAYER_NV_optimus=non_NVIDIA_only'       # Use for Vulkan (but reports non-NVIDIA GPU to app)

# Unreal Engine 4
alias unreal-editor='GDK_BACKEND=x11 __NV_PRIME_RENDER_OFFLOAD=0 __VK_LAYER_NV_optimus=NVIDIA_only MANGOHUD=0 /home/waimus/Documents/Unreal\ Engine/dev-4.27.2/Engine/Binaries/Linux/UE4Editor'

# MESA-INTEL: warning: Performance support disabled, consider sysctl dev.i915.perf_stream_paranoid=0
alias streamparanoia='sudo sysctl dev.i915.perf_stream_paranoid=0'

# Suckless' surf needs to run with X11
alias surf='GDK_BACKEND=x11 command surf'

# Godot Flatpak
alias godot='__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia flatpak run org.godotengine.Godot'

# Pipewire-Jack
alias pw-jack='PIPEWIRE_LATENCY="128/48000" command pw-jack'

# Firejail aliases
alias obs='firejail --noprofile --net=none obs'

# Cat -> highlight
alias cat='bat'

# Steam Locomotive
alias sl='sl -aln'

# GNOME Builder
alias gnome-builder='flatpak run org.gnome.Builder'

# Kate / Qt app: launch with X11
alias kate='QT_QPA_PLATFORM=xcb command kate'

###########
# STARTUP #
###########
fish_config theme choose "Tomorrow Night Bright"
/home/waimus/.local/bin/colorpanes.sh
/home/waimus/.local/bin/catpr list --count

