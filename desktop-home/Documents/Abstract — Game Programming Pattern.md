Draft. April 2024.

Paper title:

> Penerapan Design Pattern dalam Prosess Pengembangan Game Hunting Party

Game title:

 > Hunting Party of the Eternal Blue Sky

---

# Introduction

Di dalam bukunya The Logic of Scientific Discovery, Karl Popper berargumen bahwa pengetahuan memiliki sifat tidak pasti (Popper, 1934). Oleh karena itu sebuah ketidakpastian memerlukan penalaran induktif dan deduktif (Fjelland, 2010; Popper, 1934). Melalui proses tersebut, manusia dapat mengenali pola-pola yang muncul pada permasalahannya. Karena salah satu kerja otak manusia adalah dengan mengenali sebuah pola (Bor, 2012). 

Ketika manusia mengenali pola-pola baru pada berbagai macam kejadian yang terjadi, pengetahuan ini dapat digunakan untuk mengantisipasi kejadian yang akan dihadapi. Sehingga prosesnya dapat terjadi sesuai dengan usaha yang diterapkan berdasarkan pola-pola yang dikenal tanpa perlu mengingat detail-detail yang terlalu spesifik (Bor, 2012).

Begitu pula dalam penyelesaian masalah pada ilmu komputer dan pemrograman komputer secara umum. Untuk menghindari suatu struktur program yang sulit ditelusuri dalam proses troubleshooting, design pattern (pola desain sistem program) yang terbukti dan teruji dapat digunakan untuk membantu mengorganisasi dan menstrukturkan kerangka program (Wang dan Huang, 2009). Penggunaan ulang design pattern juga dapat mempercepat proses pengembangan program (Bishop, 2007).

Design pattern menyediakan kerangka dasar dalam mendesain suatu sistem dan memberikan gambaran untuk mempertimbangkan masalah-masalah yang dapat terjadi, serta gambaran dasar terhadap cara menghadapi masalah tersebut (Bishop, 2007).

Design pattern dalam konteks pengembangan game "Hunting Party of the Eternal Blue Sky" adalah penerapan pola desain sistem yang umum digunakan pada masalah dalam konteks computer science pada umumnya. Namun pada kasus ini, design pattern serupa akan diterapkan pada masalah-masalah yang dihadapi dalam proses pengembangan game "Hunting Party of the Eternal Blue Sky" via gameplay framework yang disediakan oleh Unreal Engine.

Sesuai namanya, gameplay framework adalah koleksi utilitas yang menyediakan fondasi dalam mengembangkan gameplay dengan Unreal Engine (Epic Games, 2024a). Oleh karena itu, user tidak perlu mengembangkan sistem-sistem dasar yang diperlukan oleh sebuah game seperti manajemen dan definisi struktur data, manajemen input mapping, manajemen dan definisi tipe-tipe objek dasar dalam game, dan implementasi sistem dasar lainnya (Looman, 2023).

Contoh-contoh design pattern yang dapat diterapkan dengan menggunakan gameplay framework di Unreal Engine adalah state machine, singleton, dan observer pattern. Konsep state machine dapat diterapkan sebagai metode flow control dalam mengatur alur pergantian level, alur pergantian game mode, dan lain-lain. Hal ini dapat diterapkan menggunakan GameInstance dan GameMode yang merupakan bagian dari komponen gameplay framework di Unreal Engine. Sedangkan contoh lainnya adalah mengatur alur transisi animasi karakter dengan mengunakan state macine pada Animation Blueprint di Unreal Engine (Epic Games, 2024b).

Sesuai namanya juga, GameInstance merupakan sebuah subsistem Unreal Engine yang akan selalu aktif selama game berjalan terlepas dari perubahan level yang aktif. Karakteristik GameInstance ini sama dengan konsep singleton.

Konsep observer pattern secara umum digunakan pada event system atau sistem notifikasi di sebuah program, yaitu sebuah objek menyampaikan sebuah informasi dalam bentuk event atau notifikasi tanpa melihat apa akan ada objek yang menerima atau tidak (Shvets, 2021). Dalam konteks Unreal Engine, konsep ini  digunakan sebagai event system pada setiap actor (object) dalam game. Implementasinya termasuk collision events, UI events, event saat perubahan data, dan lain-lain.

Adapun inspirasi non-teknis yang melatarbelakangi ide penciptaan game Hunting Party adalah gameplay panahan kuda pada game seperti Mount & Blade (2022) dan Kingdom Come: Deliverance (2018). Game tersebut memiliki implementasi aksi panahan kuda sebagai gameplay yang menarik, walau dengan beberapa kekurangan.

Dari konsep panahan kuda ini, inspirasi lain yang ditarik pada penciptaan karya ini adalah orang-orang Scythia/Saka di padang stepa Eurasia yang merupakan orang-orang pertama yang mulai menunggangi kuda sejak sekitar abad ke-9 SM (Davis, 2021a). Sebagai sumber protein sehari-hari dalam bertahan hidup di padang stepa Eurasia, mereka bergantung dengan berburu hewan-hewan yang ada pada kondisi geografisnya. Tradisi berburu ini terus dilakukan oleh orang-orang nomaden dengan berbagai etnis lain, termasuk Mongol (Cartwright, 2019).

Ikhtisar yang telah dijabarkan sebelumnya membuahkan ide-ide yang terkait dengan konsep presentasi dan metode pengembangan game "Hunting Party". Game ini akan bergenre action-adventure terkait presentasinya di mana pemain akan menjadi seorang pemanah kuda dan berkeliling padang stepa untuk berburu.

---

Kata kunci: design pattern, game design, unreal engine, gameplay framework


# Landasan Teori & Terminologi

| Context/field | Terms |
|---------------|-------|
| Computer science | software, framework, pemrograman, design pattern, struktur data, class |
| Game development | game, gameplay, game engine, behaviour tree, non-playable characters |
| Unreal Engine's specific terminology | gameplay framework, GameInstance, GameMode, Blueprint, |
| Latar Historis | Scythia/Saka, Massagetae, hunting party, hubungan manusia dan kuda, tradisi panahan kuda di kebudayaan orang-orang nomaden, Gaya hidup nomaden-pastoralisme |
| Latar Geografis | Padang Stepa Eurasia, Pegunungan Altai |

---

# Note doodles

> Mention about any examples of application of each pattern.

>> State Machine: application flow control (level loading, game mode state control, etc.) using Unreal's GameInstance and GameMode, and animation state machine with Animation Blueprint

>> Behaviour Tree: NPC behaviour design with Unreal's Behaviour Tree

>> Singleton: application of Unreal's GameInstance as a singleton to provide applicaiton-wide data access.

>> Observer: Event-based actions provided by the Unreal API (collision overlap event, game state events, custom events)
