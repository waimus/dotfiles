# gsettings path & key saved here
# to save horizontal space
path='org.gnome.desktop.peripherals.touchpad'
key='send-events'

# get current gsettings value
enable=$(gsettings get $path $key)

if [ $enable == "'enabled'" ]
then
  gsettings set $path $key 'disabled'
  echo "touchpad disabled"
else
  gsettings set $path $key 'enabled'
  echo "touchpad enabled"
fi
