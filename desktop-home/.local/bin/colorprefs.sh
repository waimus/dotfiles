#!/usr/bin/env bash
#
# colorprefs.sh
# Switch between dark/light preferences & themes
#
# This sets up the following stuff hence requires:
#   * mutter-rounded (WM Patch)
#   * gnome-shell-extension-user-theme (Shell extension)
#   * adw-gtk3 (GTK3 Theme)
#   * gedit merbivore & solarized-light theme

dark() {
    echo "changing appearance to dark mode"
    gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
    gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3-dark'
    gsettings set org.gnome.mutter border-width 1
    
    # Miscellaneous
    gsettings set org.gnome.gedit.preferences.editor scheme 'merbivore'
    gsettings set org.gnome.shell.extensions.user-theme name 'Default'
}

light() {
    echo "changing appearance to light mode"
    gsettings set org.gnome.desktop.interface color-scheme 'default'
    gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3'
    gsettings set org.gnome.mutter border-width 0
    
    # Miscellaneous
    gsettings set org.gnome.gedit.preferences.editor scheme 'solarized-light' 
    gsettings set org.gnome.shell.extensions.user-theme name 'Default-light'
}

help() {
    echo "Usage:"
    echo "  colorprefs.sh [OPTION]"
    echo "Options:"
    echo "  -l  switch to light mode"
    echo "  -d  switch to dark mode"
    echo "  -h  show this help"
}

# Get the options
while getopts ":dlh" option; do
   case $option in
      d) # Dark mode
         dark
         exit;;
      l) # Light mode
         light
         exit;;
      h) # Help
         help
         exit;;
     \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done

help
