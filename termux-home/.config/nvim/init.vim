" Set command history limit
set history=100

" Turn on line numbers
set number

" Enable syntax highlightning
syntax on

" Style
set termguicolors
colorscheme gruvbox

" Tab/indentation setting
set tabstop=4
set shiftwidth=4
set expandtab

set mouse=nv





