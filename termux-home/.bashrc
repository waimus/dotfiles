# Environment
export PATH=$HOME/.local/bin:$PATH
export SD=/sdcard
export USER=waimus
export HOSTNAME=ricepod

# Alias
# The neofetch ASCII used here is available at https://github.com/waimus/ascii-logo
alias neofetch='neofetch --ascii $HOME/.ascii/lp-dots.txt --colors 3 3 15 12 --ascii_colors 11 15 12'

alias la='ls -a'

# Prompt
C1="\[$(tput setaf 12)\]"
C2="\[$(tput setaf 3)\]"
RESET="\[$(tput sgr0)\]"

user="[${C2}$USER@$HOSTNAME${RESET}]"
wd="${C1}\w${RESET}"

PS1="${user} ${wd}\n$ "

# Startup
neofetch







