## dotfiles
Ingredients:
- `desktop-home` are configs for my desktop.
- `termux-home` are config for Termux in my phone.
- Firefox `customChrome.css`
- oh-my-zsh config ~ with custom theme based on `alanpeabody` theme
- Neovim init config ~ includes [gruvbox theme](https://github.com/morhetz/gruvbox)
- Neofetch config
- Custom ASCII art probably for Neofetch
- ~~This is 10% bash, 20% zsh, 15% vimscript, 5% rice, 50% risotto, an a 100% reason to remember the name~~
